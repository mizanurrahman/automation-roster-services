package com.scholastic.sps.roster.tests;

import org.junit.Before;

import com.jayway.restassured.RestAssured;

public class BaseTest 
{
	@Before
	public void init() 
	{
		//RestAssured.baseURI = System.getProperty("baseURI");
		RestAssured.baseURI="http://schws.qa2.scholastic.com";


	//	RestAssured.port = Integer.parseInt(System.getProperty("port"));
		RestAssured.port=80;
		
		//RestAssured.basePath="/app/sps-lookup/1.0";
		RestAssured.basePath = "/SchCXFWS/services";
		
		System.out.println("\n\n\n\n*******Initialized RestAssured*******");
		System.out.println("RestAssured.baseURI: " + RestAssured.baseURI);
		System.out.println("RestAssured.port: " + RestAssured.port);
		System.out.println("RestAssured.basePath: " + RestAssured.basePath); 
		System.out.println("******************************\n\n\n\n");
	}	

}
